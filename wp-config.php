<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'practice');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gFB.Y#5V2is$I9GYcjtk1v ~e-Pr}RGMd lt_&mc!0T)9Kh{U/9~tHx:YyUfZY1g');
define('SECURE_AUTH_KEY',  'mHzoi]`o^s)y(1!aE97cfgh)=)_jj~CA1r%cf%SY2+AX;=]^PY&XN6L1}oRuk^De');
define('LOGGED_IN_KEY',    'kc/257,EJ]A T(%[$T2UB]([%7usln*#}N{ #E(|g9;ZArZg(6N=T$ rv727[oq$');
define('NONCE_KEY',        ':/M_16_D(mMCv|U:w(he: qLGUNf#;?HhqXALymr`S~r_qJY9pASm3>Ubc_zo=%d');
define('AUTH_SALT',        'C2,?-L5}I6AwuHeS-:;hTF$6I3<5q(rZ(bC1eweBfsI_[6^d%>,}^^<4F]a*QRxZ');
define('SECURE_AUTH_SALT', '2or$y%EQq.hBPy1pLl8@$y)4xbi P:JEr$;8NE)d/qM$GpP1 O)GNE?yG 4Xq*oK');
define('LOGGED_IN_SALT',   '~7o ExOd1<`ijOs7|[g.A9/Fgn1%(g1=p5(=%mi%TRU<8Ke9u~`*FsMz$Krah]L%');
define('NONCE_SALT',       '{9*ii9&WXL9rr8JOe~a|s+S>Cg0P3aU<}GV2:A;CZLGMHDOKI;_H({@1m*W0<(=h');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
